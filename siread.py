#!/usr/bin/env python3

import click
from si7021 import Si7021
from time import sleep
from smbus import SMBus
import requests
import logging
import click_log

logger = logging.getLogger(__name__)
click_log.basic_config(logger)

def gettemp():
    sensor = Si7021(SMBus(1))
    return sensor.read()

def senddata(name, type, data):
    r = requests.get(f'http://gw/templog/add/{name}/{type}/{data}')
    code = r.status_code
    logger.debug(f'We sent in the data {data:.1f} of type {type} under the name {name}, resulting in the code {code:d}.')
    return code

@click.command()
@click_log.simple_verbosity_option(logger)
def hello():
    logger.debug('Hello world!')
    humi,temp = gettemp()
    logger.debug(f'Data: {humi:.1f} %, {temp:.1f} C')
    senddata('assault', 'hum', humi)
    senddata('assault', 'temp', temp)

if __name__ == '__main__':
    hello()


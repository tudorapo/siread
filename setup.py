from setuptools import setup

setup(
    name='siread',
    version='0.0.0',
    py_modules=['siread'],
    install_requires=[
        'Click',
        'Si7021',
        'smbus',
        'requests',
        'click_log',
    ],
    entry_points='''
        [console_scripts]
        siread=siread:hello
    ''',
)
